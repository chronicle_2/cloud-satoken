package com.wangchenyang.common.satoken.core.dao;

import cn.dev33.satoken.dao.SaTokenDao;
import cn.dev33.satoken.util.SaFoxUtil;
//import com.wangchenyang.common.redis.utils.RedisUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Sa-Token持久层接口(使用框架自带RedisUtils实现 协议统一)
 *
 */
@RequiredArgsConstructor
@Slf4j
public class SaTokenDaoImpl implements SaTokenDao {

	private final RedisTemplate<String, Object> redisTemplate;

	/**
	 * 获取Value，如无返空
	 */
	@Override
	public String get(String key) {
//		return RedisUtils.getCacheObject(key);
		return (String) redisTemplate.opsForValue().get(key);
	}

	/**
	 * 写入Value，并设定存活时间 (单位: 秒)
	 */
	@Override
	public void set(String key, String value, long timeout) {
		if (timeout == 0 || timeout <= SaTokenDao.NOT_VALUE_EXPIRE) {
			return;
		}
		// 判断是否为永不过期
		if (timeout == SaTokenDao.NEVER_EXPIRE) {
//			RedisUtils.setCacheObject(key, value);
			redisTemplate.opsForValue().set(key, value);
		}
		else {
//			RedisUtils.setCacheObject(key, value, Duration.ofSeconds(timeout));
			redisTemplate.opsForValue().set(key, value, Duration.ofSeconds(timeout));
		}
	}

	/**
	 * 修修改指定key-value键值对 (过期时间不变)
	 */
	@Override
	public void update(String key, String value) {
		long expire = getTimeout(key);
		// -2 = 无此键
		if (expire == SaTokenDao.NOT_VALUE_EXPIRE) {
			return;
		}
		this.set(key, value, expire);
	}

	/**
	 * 删除Value
	 */
	@Override
	public void delete(String key) {
		redisTemplate.delete(key);
	}

	/**
	 * 获取Value的剩余存活时间 (单位: 秒)
	 */
	@Override
	public long getTimeout(String key) {
//		long timeout = RedisUtils.getTimeToLive(key);
		long timeout = redisTemplate.getExpire(key);
		return timeout < 0 ? timeout : timeout / 1000;
	}

	/**
	 * 修改Value的剩余存活时间 (单位: 秒)
	 */
	@Override
	public void updateTimeout(String key, long timeout) {
		// 判断是否想要设置为永久
		if (timeout == SaTokenDao.NEVER_EXPIRE) {
			long expire = getTimeout(key);
			if (expire == SaTokenDao.NEVER_EXPIRE) {
				// 如果其已经被设置为永久，则不作任何处理
			}
			else {
				// 如果尚未被设置为永久，那么再次set一次
				this.set(key, this.get(key), timeout);
			}
			return;
		}
		redisTemplate.opsForValue().getAndExpire(key, Duration.ofSeconds(timeout));
//		RedisUtils.expire(key, Duration.ofSeconds(timeout));
	}

	/**
	 * 获取Object，如无返空
	 */
	@Override
	public Object getObject(String key) {
//		return RedisUtils.getCacheObject(key);
		return redisTemplate.opsForValue().get(key);
	}

	/**
	 * 写入Object，并设定存活时间 (单位: 秒)
	 */
	@Override
	public void setObject(String key, Object object, long timeout) {
		if (timeout == 0 || timeout <= SaTokenDao.NOT_VALUE_EXPIRE) {
			return;
		}
		// 判断是否为永不过期
		if (timeout == SaTokenDao.NEVER_EXPIRE) {
//			RedisUtils.setCacheObject(key, object);
			redisTemplate.opsForValue().set(key, object);
		}
		else {
//			RedisUtils.setCacheObject(key, object, Duration.ofSeconds(timeout));
			redisTemplate.opsForValue().set(key, object, Duration.ofSeconds(timeout));
		}
	}

	/**
	 * 更新Object (过期时间不变)
	 */
	@Override
	public void updateObject(String key, Object object) {
		long expire = getObjectTimeout(key);
		// -2 = 无此键
		if (expire == SaTokenDao.NOT_VALUE_EXPIRE) {
			return;
		}
		this.setObject(key, object, expire);
	}

	/**
	 * 删除Object
	 */
	@Override
	public void deleteObject(String key) {
//		RedisUtils.deleteObject(key);
		redisTemplate.delete(key);
	}

	/**
	 * 获取Object的剩余存活时间 (单位: 秒)
	 */
	@Override
	public long getObjectTimeout(String key) {
//		long timeout = RedisUtils.getTimeToLive(key);
		long timeout = redisTemplate.getExpire(key);
		return timeout < 0 ? timeout : timeout / 1000;
	}

	/**
	 * 修改Object的剩余存活时间 (单位: 秒)
	 */
	@Override
	public void updateObjectTimeout(String key, long timeout) {
		// 判断是否想要设置为永久
		if (timeout == SaTokenDao.NEVER_EXPIRE) {
			long expire = getObjectTimeout(key);
			if (expire == SaTokenDao.NEVER_EXPIRE) {
				// 如果其已经被设置为永久，则不作任何处理
			}
			else {
				// 如果尚未被设置为永久，那么再次set一次
				this.setObject(key, this.getObject(key), timeout);
			}
			return;
		}
//		RedisUtils.expire(key, Duration.ofSeconds(timeout));
		redisTemplate.opsForValue().getAndExpire(key, Duration.ofSeconds(timeout));
	}

	/**
	 * 搜索数据
	 */
	@Override
	public List<String> searchData(String prefix, String keyword, int start, int size) {
//		Collection<String> keys = RedisUtils.keys(prefix + "*" + keyword + "*");
		Collection<String> keys = redisTemplate.keys(prefix + "*" + keyword + "*");
		List<String> list = new ArrayList<>(keys);
		return SaFoxUtil.searchList(list, start, size);
	}

}
